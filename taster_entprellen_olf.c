/*
 * UebungenSDK500.c
 *
 * Created: 27.05.2013 15:56:25
 *  Author: olf
 */ 

#include <avr/io.h>
#include <util/delay.h>

int main(void)
{
	DDRB  = 0xFF;
	PORTB = 0xFF;	
	uint8_t ledValues = 0xFE;
	
	DDRA = 0x00;
	PORTA = 0xFF;
	uint8_t tasterValues = PORTA;
	
    while(1)
    {		
		tasterValues = ~PINA;
        ledValues = ~PORTB;
		//ledValues = 0xFF;
		if (tasterValues != 0x00) {
			ledValues += tasterValues;
		}
		_delay_ms(1000); 
		PORTB = ~ledValues;
		
    }
}