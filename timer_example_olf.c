#include <avr/interrupt.h> 
#include <avr/io.h>
#include <util/delay.h>

int counter = 0x00;

ISR(TIMER1_COMPA_vect) {
	counter++;
	PORTB = ~(counter++);	
}

int main(void)
{
	DDRB  = 0xFF;
	PORTB = 0xFF;
	/*TCCR0 = (1<<CS01);
	TIMSK |= (1<<TOIE0);*/
	TIMSK = (1<<OCIE1A);
	TCCR1A = (1 << COM1A0) | (1 << COM1B0);
	TCCR1B = (1 << CS12);
	//OCR1A = 0b01111010;
	//OCR1B = 0b00010010;
	OCR1A = 1500;
	//OCR1B = 0b00000000;
	sei();
	
	
	while(1)  {
		;
	}
}