/*************************************************************************
* Plays song using FAST PWM and Interrupt, short program
* Connect speaker to PD5 and Ground
**************************************************************************/

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

//define F_CPU 8000000uL

#define PRESCALER       32
#define HZ_TO_TIMER(hz) (F_CPU / PRESCALER / hz)

#define TON_0   0
#define TON_C1  HZ_TO_TIMER(261)
#define TON_D1  HZ_TO_TIMER(293)
#define TON_E1  HZ_TO_TIMER(329)
#define TON_F1  HZ_TO_TIMER(349)
#define TON_G1  HZ_TO_TIMER(392)
#define TON_A1  HZ_TO_TIMER(440)
#define TON_H1  HZ_TO_TIMER(493)

#define DUR_1  (1000000 / 255 /2)
#define DUR_2  (1000000 / 255 / 4)
#define DUR_4  (1000000 / 255 / 8)



/** store each note */
struct note {
   uint16_t ton;         // is devided by 4 to fit uint8_t !!!!
   uint16_t duration;    // is devided by 4 to fit uint8_t !!!!
};

struct note entchen[] = {
   {TON_C1, DUR_4},
   {TON_D1, DUR_4},
   {TON_E1, DUR_4},
   {TON_F1, DUR_4},
   {TON_G1, DUR_2},
   {TON_G1, DUR_2},
   
   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_G1, DUR_2},

   {TON_0, DUR_4},  

   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_A1, DUR_4},
   {TON_G1, DUR_2},

   {TON_F1, DUR_4},
   {TON_F1, DUR_4},
   {TON_F1, DUR_4},
   {TON_F1, DUR_4},
   {TON_E1, DUR_2},
   {TON_E1, DUR_2},

   {TON_D1, DUR_4},
   {TON_D1, DUR_4},
   {TON_D1, DUR_4},
   {TON_D1, DUR_4},
   {TON_C1, DUR_1}
};

/** setup the timer */
void playInit() {
   TCCR1A |= (1 << WGM11) | (1 << WGM10);    // FAST PWM, Top = OCR1A 
   TCCR1B |= (1 << WGM13) | (1 << WGM12);    // FAST PWM
   TCCR1A |= (1 << COM1A0);		             // Toggle OC1A on compare match, Output on OC1A = PD5-PIN
   TCCR1B |= (1 << CS11);                    // clk(I/O)/8 (From prescaler)
   TIMSK  |= (1 << OCIE1A);                  // OCIE1A: Output Compare A Match Interrupt Enable
   
   OCR1A = 0; //sofort in ISR
   sei();                                     //SREG |= 0x80;
}


ISR(TIMER1_COMPA_vect) {        
	static int noteCtr = 0;
	static int currentDuration = 0; 

	if(currentDuration == 0){
		OCR1A = entchen[noteCtr].ton;
		currentDuration = entchen[noteCtr].duration;
	    if(noteCtr++ == 28){
			noteCtr = 0;
		}
	} 
	currentDuration--;
}


/** main */
int main(void) {
   DDRD = 0xFF;   //for PD5
   playInit();
	while(1);
   return 0;
}

