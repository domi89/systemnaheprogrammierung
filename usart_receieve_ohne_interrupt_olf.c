#include <avr/io.h>
#include <util/delay.h>

#ifndef F_CPU
#define F_CPU 8000000
#endif
#define UART_BAUD_RATE 9600
#define UART_FACTOR (F_CPU / (16 * UART_BAUD_RATE))


void USART_Init(unsigned int baud) {
	UBRRH = (unsigned char)(baud>>8); // Set baud rate 
	UBRRL = (unsigned char)baud;
	UCSRB = (1<<RXEN)|(1<<TXEN);  // Enable rx and tx 
	UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0); // Set frame format: 8data, 2stop bit 
}


void USART_Transmit(unsigned char data) {
	// Wait for empty transmit buffer 
	while( !( UCSRA & (1<<UDRE)) );
	// Put data into buffer, sends the data 
	UDR = data;
}

unsigned char USART_Receive(void) {
	// Wait for data to be received 
	while( !(UCSRA & (1<<RXC)) )
	;
	// Get and return received data from buffer 
	return UDR;
}

int main(void) {
	USART_Init(UART_FACTOR);
	DDRB  = 0xFF;
	PORTB = 0xFF;
	while(1) {
		PORTB = USART_Receive();
	}
}