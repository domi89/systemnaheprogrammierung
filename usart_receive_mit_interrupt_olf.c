#include <avr/interrupt.h> 
#include <avr/io.h>
#include <util/delay.h>

#ifndef F_CPU
#define F_CPU 8000000
#endif
#define UART_BAUD_RATE 9600
#define UART_FACTOR (F_CPU / (16 * UART_BAUD_RATE))

void USART_Init(unsigned int baud) {
	UBRRH = (unsigned char)(baud>>8); // Set baud rate 
	UBRRL = (unsigned char)baud;
	UCSRC = (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0); // Asynchron 8N1
	UCSRB |= (1<<RXEN)|(1<<TXEN)|(1<<RXCIE);  // UART RX, TX und RX Interrupt einschalten
}


void USART_Transmit(unsigned char data) {
	// Wait for empty transmit buffer 
	while( !( UCSRA & (1<<UDRE)) );
	// Put data into buffer, sends the data 
	UDR = data;
}

unsigned char USART_Receive(void) {
	// Wait for data to be received 
	while( !(UCSRA & (1<<RXC)) )
	;
	// Get and return received data from buffer 
	return UDR;
}

ISR(USART_RX_vect)
{
	//if(USART_RX_vect){
		PORTB = UDR;
	//}
	
}

int main(void) {
	
	USART_Init(UART_FACTOR);
	DDRB  = 0xFF;
	PORTB = 0xFF;
	int counter = 0xFF;
	sei();
	while(1) {
		//PORTB = USART_Receive();
		//USART_Transmit(0x
		counter--;
		USART_Transmit(counter)
		;
	}
}