#include <avr/io.h>    
#include <avr/interrupt.h>

int i = 0;							//Z�hlvariable f�r Interrupts
int j = 0;							//Z�hlvariable f�r Uhrzeit

int main (void) 
{            
	DDRB  = 0xFF;   				//Port B auf Ausgang setzen       

  	TCCR0 = (1<<CS02) | (1<<CS00); 	//Prescaler auf 1024 setzen 
								   	//3686000Hz/1024 = 3600Hz, so schnell z�hlt der Timer
									//3600Hz / 256 = 14,1Hz, so oft produziert der Timer einen Overflow
 					
  	TIMSK |= (1<<TOIE0);			//Timer Overflow Interrupt erlauben
 
  	sei();							//Interrupts generell zulassen

  	while(1)						//Endlosschleife
  	{}
	//end while

   	return 0;
}//end method main

ISR (TIMER0_OVF_vect)				//wird ca. 14 Mal in der Sekunde aufgerufen
{
	i++;							//Z�hlvariable f�r Interruptanzahl hochz�hlen

	if (i == 14) 					//wenn Interrupt zum 14. Mal aufgerufen wurde			
	{	
		i = 0;						//Z�hlvariable zur�cksetzen

		if (j>=60)					//wenn 60 Sekunden erreicht
		{
			j = 0;					//j wieder auf 0 setzen
		}//end if
		
		j = ~j;						//j invertieren, da Ausg�nge active low
		PORTB = j;					//j auf Port B legen
		j = ~j;						//j wieder invertieren, damit weitergez�hlt werden kann
		j++;						//Uhrzeit-Variable um eins erh�hen			
	}//end if  
}//end method ISR

