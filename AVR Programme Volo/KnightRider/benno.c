#include <avr/io.h>    
#include <avr/interrupt.h>
//#include "uart.h"
//#include "music.h"
#include "ohdufroehliche.h"

#define IRQ_FRQ 31250 //= Timer 0 ohne Prescaler: 8000000/256

unsigned char i = 0; //zahl der isr aufrufe
unsigned char j = 0; //position im lied
unsigned int k = 0; //zahl der schwingungen um tonlaenge zu bestimmen
unsigned char pause; //synth. frequenz f�r pause

Note note; //gerade zu spielende note

void setup(); //fwd dekl, frquenzen anpassen
void play_song();

Note pnote;

int main (void) {

	DDRB  = 0xFF; 	//speaker an pin 0
	DDRC  = 0xFF; 	//leds	

	PORTC  = 0x00; 	//leds
	// Timer 0: pescaler aus, overflow irq ein
	TCCR0 = (1<<CS00); 
	TIMSK |= (1<<TOIE0);	

//	init_uart();

	setup();

	pnote.pitch = pause;
	pnote.length = LG;
	pnote.compfrq = pause;
 
	if (SONGLENGTH > 0)
	{
		note = notes[0];
	} else {
		note = pnote;
	}
	sei();

	while(1) 
	{
		//PORTC = uart_getc();
	
	}

	return 0;
}


//tonfrequenzen an die irq frequenz anpassen und tonlaengen berechnen
void setup()
{
	int g;

	for (g = 0; g < SONGLENGTH; g++)
	{
		notes[g].compfrq = notes[g].pitch * (notes[g].length + 1) / 4;
	}

	for (g = 0; g < SONGLENGTH; g++)
	{
		notes[g].pitch = (IRQ_FRQ/notes[g].pitch)/2;
	}
	pause = IRQ_FRQ/P/2;

}


void play_song()
{
	i++;

	if (j == SONGLENGTH) {
		j = 0;
		note = notes[j];
	}

	if (i == note.pitch) {
		k++;
		i = 0;
		if (k >= note.compfrq) {
			k = 0;
			note = notes[++j];
			if (note.pitch == pause)
			{
				PORTB = 0xFE;
			}
		} else {
			if (note.pitch != pause)
			{
				PORTB ^= 1;
			}
		}
	}
}


void play_note()
{
	i++;

	if (i == note.pitch) {
		k++;
		i = 0;
		if (k >= note.compfrq) {
			k = 0;
			note = pnote;
			if (note.pitch == pause)
			{
				PORTB = 0xFE;
			}
		} else {
			if (note.pitch != pause)
			{
				PORTB ^= 1;
			}
		}
	}
}

ISR (TIMER0_OVF_vect)
{
	if (SONGLENGTH > 0)
	{
		play_song();
	} else {
		play_note();
	}
}

ISR (USART_RX_vect) 
{
	unsigned char c = UDR;
	if (c > 96 && c < 123)
	{
		note = notes[c - 97];
		k = 0;
		i = 0;
	}
}
