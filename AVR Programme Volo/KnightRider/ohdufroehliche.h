#ifndef OHDUFROEHLICHE_H_
#define OHDUFROEHLICHE_H_

#include "toene.h"

#define SONGLENGTH 45

Note noten[] = 
{
		/*ertse zeile*/
		{ G1, LH }, { A1, LH }, { G1, LVP }, { F1, LA }, { E1, LV }, { F1, LV }, { G1, LH }, { A1, LH }, { G1, LVP }, { F1, LA }, { E1, LV }, { F1, LV },
		
		/*zweite Zeile*/
		{ G1, LH }, { G1, LH }, { A1, LH }, { H1, LV }, { C2, LV }, { H1, LH }, { A1, LH }, { G1, LG },

		/*dritte Zeile*/
		{ D1, LVP }, { E1, LA }, { D1, LV }, { E1, LV }, { F1, LVP }, { G1, LA }, { F1, LH }, { E1, LVP }, { F1, LA }, { E1, LV }, { F1, LV }, { G1, LVP }, { A1, LA }, { G1, LH },

		/*vierte Zeile*/
		{ C2, LV }, { H1, LV }, { A1, LV }, { G1, LV }, { C2, LV }, { A1, LV }, { G1, LV }, { F1, LV }, { E1, LH }, { D1, LH }, { C1, LG }	  
};

#endif
