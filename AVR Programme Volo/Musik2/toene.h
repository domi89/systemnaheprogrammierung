#ifndef TOENE_H_
#define TOENE_H_

/*Tonhöhe*/

#define AIS0 	238
#define H0 		248
#define HIS0 	256
#define C1 		264
#define CIS1 	275
#define D1 		297
#define DIS1 	317
#define E1 		330
#define F1 		352
#define G1 		396
#define A1 		440
#define H1 		495
#define C2 		528
#define P         1 


/*Tonlänge*/ 

#define LG 32 	/*Ganze*/
#define LH 16   /*Halbe*/
#define LVP 12  /*verlängerte Viertel*/
#define LV 8	/*Viertel*/
#define LA 4	/*Achtel*/
#define LS 2	/*Sechzehntel*/
#define LZ 1    /*Zweiunddreißigstel*/

typedef struct Note 
{
	unsigned int pitch : 12;
	unsigned char length : 4;
	unsigned int compfrq : 16;
} Note;

#endif
