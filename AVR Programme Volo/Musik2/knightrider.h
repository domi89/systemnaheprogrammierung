#ifndef KNIGHTRIDER_H_
#define KNIGHTRIDER_H_

#include "toene.h"

#define SONGLENGTH 60

Note noten[] = 
{
		/*ertse zeile*/
		{ CIS1, LA }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { CIS1, LS }, { HIS0, LS }, { CIS1, LS }, { CIS1, LS }, { CIS1, LS }, 
		{ CIS1, LA }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { D1, LS }, { CIS1, LS }, { CIS1, LS }, { CIS1, LS }, { HIS0, LS }, { CIS1, LS }, { CIS1, LS }, { CIS1, LS }, 
		/*zweite zeile*/
		{ H0, LA }, { C1, LS }, { H0, LS }, { H0, LS }, { C1, LS }, { H0, LS }, { H0, LS }, { C1, LS }, { H0, LS }, { H0, LS }, { H0, LS }, { AIS0, LS }, { H0, LS }, { H0, LS }, { H0, LS }, 
		{ H0, LA }, { C1, LS }, { H0, LS }, { H0, LS }, { C1, LS }, { H0, LS }, { H0, LS }, { C1, LS }, { H0, LS }, { H0, LS }, { H0, LS }, { AIS0, LS }, { H0, LS }, { H0, LS }, { H0, LS }
};

#endif
