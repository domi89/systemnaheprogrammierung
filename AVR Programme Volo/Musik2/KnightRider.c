#include <avr/io.h>    
#include <avr/interrupt.h>
#include "initial.h"
#include "knightrider.h"

#define IR_FRQ 31500

unsigned int i = 0;					//Z�hlvariable f�r Interrupt-Anzahl Notenfrequenz
unsigned char richtung = 0;			//Richtung des Lauflichts
unsigned int j = 0;					//Index f�r aktuelle Note
unsigned int k = 0;					//Z�hlvariable f�r Interrupt-Anzahl Notenl�nge
unsigned int m = 0;					//Z�hlvariable f�r Interrupt-Anzahl Lauflicht
unsigned char ledzaehler = 0;		//Variable f�r 8bit-Wert Lauflicht			
unsigned char pause;				//Variable f�r PAUSE
Note note;						    //aktuelle Note

int main (void) 
{            
	noten_berechnen();				//Notenfrequenzen berechnen
	
	DDRB  = 0xFF;   				//Port B auf Ausgang setzen
	PORTB = 0x01;					//Pin1 an PortB aufleuchten lassen  
	
	DDRD  = 0xFF;   				//Port D auf Ausgang setzen

	TCCR0 = (1<<CS00); 				//Prescaler f�r Timer 0 disablen
								 					
  	TIMSK = (1<<TOIE0);				//Timer Overflow Interrupt erlauben

	sei();							//Interrupts generell zulassen

		
	while(1)						//Endlosschleife
  	{
	}//end while

   	return 0;
}//end method main

/*berechnet die Notenfreqenz und -l�nge*/
void noten_berechnen()
{
	unsigned int songnote;																		//Variable f�r aktuelle Note

	//f�r jede Note im Song
	for (songnote = 0; songnote < SONGLENGTH; songnote++)										
	{
		noten[songnote].compfrq = noten[songnote].pitch * (noten[songnote].length + 1) / 11;	//Wert f�r Tonl�nge in Abh�ngigkeit zur Tonh�he f�r IR brechnen
		noten[songnote].pitch = (IR_FRQ/noten[songnote].pitch)/2;								//Wert zur Tonerzeugung f�r IR berechnen
	}//end for

	pause = IR_FRQ/P/2;																			//Wert f�r Pause berechnen
}//end method noten_berechnen()


void play_song()
{									
	i++;							//Z�hler f�r Frequenz erh�hen							

	//falls Ende des Songs erreicht, von vorne anfangen
	if (j == SONGLENGTH)			
	{
		j = 0;
		note = noten[j];
	}//end if

	//wenn Z�hler die umgerechnete Notenfrequenz erreicht hat
	if (i == note.pitch) 
	{
		k++;						//Z�hler f�r L�nge erh�hen
		
		i = 0;						//Z�hler f�r Frequenz auf 0
		
		//wenn Z�hler die berechnete Zahl erreicht hat
		if (k >= note.compfrq) 
		{
			k = 0;					//Z�hler f�r L�nge auf 0
			note = noten[j++];		//n�chste Note
			
			//wenn pause gespielt werden soll, nichts tun
			if (note.pitch == pause)
			{
				PORTD = 0xFE;
			}//end if
		} 
		else 
		{
			//wenn keine Pause, PORT togglen f�r Tonerzeugung
			if (note.pitch != pause)
			{
				PORTD ^= 1;
			}//end if
		}//end if
	}//end if
}//end method play_song


//ISR f�r Timer0Overflow
ISR (TIMER0_OVF_vect)				//wird 31250mal pro Sekunde aufgerufen
{
	m++;

	//wenn ISR 1700 mal aufgerufen wurde
	if (m>=1700)
	{
		ledzaehler++;				//LED-Z�hler erh�hen
		ledzaehler %=8;				//Z�hler innerhalb der Grenzen halten
		
		//wenn Lauflicht links oder rechts angekommen, Richung wechseln
		if (ledzaehler == 0)		
			richtung = ~richtung;
		
		//Licht nach links oder rechts shiften
		if (richtung == 0x00)
		{
			PORTB = (1 << (ledzaehler+1));
		}
		else
		{
			PORTB = (0b10000000 >> ledzaehler);
		}//end if

		m = 0;						//Z�hler zur�ckseten
	}//end if

	//wenn Song vorhanden, abspielen
	if (SONGLENGTH > 0)
	{
		play_song();
	}//end if
}//end ISR TIMER0_OVF
