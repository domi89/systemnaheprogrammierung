#include <avr/io.h>	
#include <avr/delay.h> 
#include <util/delay.h>

void wait(){
	_delay_ms(100000);
}


int main (void) {
            
	DDRC	= 0xFF; //C auf ausgang
	PORTC	= 0b11111111; // alle lampen aus
	uint8_t laufvariable = 1;
	int i;

	while(1){
	
		for(i=0;i<8;i++){
			PORTC=~( 1 << i);
			if(i<7)                                   
				wait();
		}

		laufvariable = 1;

		for(i=7;i>0;i--){
			PORTC=~( 1 << i);                                   
			wait();
		}
		
		
	}

	return 0;
}
