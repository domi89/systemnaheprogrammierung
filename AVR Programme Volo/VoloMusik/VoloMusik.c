#include <avr/io.h>
#include <util/delay.h>


#define F_CPU = 8000000UL

#define C 40
#define D 50
#define E 60
#define F 70
#define G 80
#define A 90
#define H 105
#define C1 120


void ton (int hoehe, int laenge, uint8_t pause) {
	
	// ist-Freq = F_CPU/(12*Soll-Freq)
	// tonl�nge = F_CPU*laenge/(12*ist-Freq)
	int i=0;
	int del = (1.0/hoehe)*500;
	int len = (2.0*laenge*hoehe);
	int paus = 200*pause;

	for (i=0; i<len; i++) {
		PORTB ^= (1<<PB0);
		_delay_ms(del);
	}
	_delay_ms(paus);
}

int main()
{
	DDRB	= 0xFF; //C auf ausgang

	while(1){
		ton(G,2,5);
		ton(G,2,5);
		ton(G,2,5);
		ton(D,1,5);
		ton(A,1,5);
		ton(G,2,5);
		ton(D,1,5);
		ton(A,1,5);
		ton(G,3,5);

	}

	return 0;
}
