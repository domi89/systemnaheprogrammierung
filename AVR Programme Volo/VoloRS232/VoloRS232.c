

#include <avr/io.h>

#include <avr/delay.h> 
#include <util/delay.h>
#include <avr/interrupt.h>



void USART_Init() {
	//UBRRH = (unsigned char)(baud>>8); /* Set baud rate */
	UBRRL = 51; //Datenblatt seite 162
	UCSRB = (1<<RXEN)|(1<<TXEN)|(1<<RXCIE); // Enable rx and tx and interrupts enabled
	UCSRC = (1<<URSEL)|(1<<UCSZ1)|(1<<UCSZ0); //URSEL ist f�r UCSRC
}


void USART_Transmit(unsigned char data) {
	/* Wait for empty transmit buffer */
	while( !( UCSRA & (1<<UDRE)) );
	/* Put data into buffer, sends the data */
	UDR = data;
}

unsigned char USART_Receive(void) {
	/* Wait for data to be received */
	while( !(UCSRA & (1<<RXC)) );
	/* Get and return received data from buffer */
	return UDR;
}

ISR(USART_RX_vect){
	PORTC = UDR;//Liest das Register UDR aus
}

void wait(){
	_delay_ms(50);
}

int main(){

	DDRC	= 0xFF; //C auf ausgang
	DDRA	= 0x00; //A auf eingang
	PORTC	= 0b11111111; // alle lampen aus
	uint8_t laufvariable = 1;
	int i;

	USART_Init();
	sei();//global interrupts anschalten
	
	while(1) {
		USART_Transmit(PINA);
	 	
		
		//USART_Receive();  //f�llt jetzt durch isr-interrupt weg

	}

	return 0;
}
