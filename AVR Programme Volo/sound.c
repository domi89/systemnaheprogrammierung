

#include <avr/io.h>
#include <util/delay.h>
#define F_CPU = 8000000UL

// ist-Freq = F_CPU/(12*Soll-Freq)
// tonl�nge = F_CPU*laenge/(12*ist-Freq)

void ton (float hz, float laenge) {
	int i=0;

	int del = (1.0/hz)*500;
	int len = (2.0*laenge*hz);

	for (i=0; i<len; i++) {
		PORTB ^= (1<<PB0);
		_delay_ms(del);
	}
}

int main() {
	
	DDRB = (1<<PB0);

	while(1) {
		ton(50,1);
		_delay_ms(80);
		ton(60,1);
		_delay_ms(80);
		ton(70,1);
		_delay_ms(80);
		ton(80,1);
		_delay_ms(80);
		ton(90,2);
		_delay_ms(80);
		ton(90,2);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(90,2);
		_delay_ms(560);

		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(110,1);
		_delay_ms(80);
		ton(90,2);
		_delay_ms(560);

		ton(80,1);
		_delay_ms(80);
		ton(80,1);
		_delay_ms(80);
		ton(80,1);
		_delay_ms(80);
		ton(80,1);
		_delay_ms(80);
		ton(70,2);
		_delay_ms(80);
		ton(70,2);
		_delay_ms(160);

		ton(120,1);
		_delay_ms(80);
		ton(120,1);
		_delay_ms(80);
		ton(120,1);
		_delay_ms(80);
		ton(120,1);
		_delay_ms(80);
		ton(50,4);
		_delay_ms(80);
		_delay_ms(2000);
	}
	return 0;
}


